Massive Clouds

At first read Document_jp.pdf or Document_en.pdf.

WebGL Demo (Please open by Google Chrome browser)
  https://mewli.st/MassiveCloudsDemo/v4.0.0/CloudsDemo/
  https://mewli.st/MassiveCloudsDemo/v4.0.0/BeyondTheCloudsDemo/


[ChangeLog]

--------------
Version 4.1.2(2021/4/5)

- [Fix] Not work on Unity2020.3 Built-in Pipeline

--------------
Version 4.1.1(2020/5/2)

- [Fix] Shader Error on specific platform
- [Fix] Light calculation on HDRP.

--------------
Version 4.1.0(2020/2/5)

- [New] UniversalRP 7.0.1 Compatible
- [New] HDRP 7.1.8 Compatible
- [New] Far Height Fog Range Adjustable
- [New] Height Fog Density Adjustable

--------------
Version 4.0.2(2019/08/22)

- [Fix] Fix incorrect HeightFog in near range

--------------
Version 4.0.1(2019/8/8)

- [Fix] Fix incorrect volumetric shadow in near range

--------------
Version 4.0.0(2019/8/4)

- [New] Authentic Renderer added
- [New] Volumetric Shadow(GodRay)
- [New] Ambient Lighting Feature
- [New] Direct Lighting Feature
- [New] Light Synchronization and adjustment
- [New] Night Lighting Feature
- [New] New Clouds Color synchronization modes
- [New] Height Adjustment UI refined
- [New] LWRP VR compatible

- [Omit] X-Ray Rendering Mode

- [Fix] Remove warnings on imported

--------------
Version 3.2.1

- Fix: VR: Wrong depth clipping with SinglePass Stereo.

--------------
Version 3.2.0

Features 
- Unlimited number of multiple layers of cloud.

--------------
Version 3.1.0

Features 
- LWRP Renderer Feature in Unity2019.1 and later ready (RendererFeature provided as unitypackage) 
- Clouds parameters are able to be controlled from script 
- Add MassiveCloudsCameraEffect component separated from MassiveClouds component. Now Camera effect set up is required MassiveCloudsCameraEffect and MassiveClouds component on Camera. 
- PPSv2 Custom effect now requires MassiveClouds component on Camera 
- Figure parameter added. Id adjust top and bottom shaping curve. 

- Fix: Lighting at near bottom or top of clouds is not correct 

--------------
Version 3.0.0 (2019/5/3)

Features
- All renderer is overhauled
- All DemoScene is renewed
- 4 Switchable Lighting Modes (Lucid/Solid/Surface/XRay)
- New Shaping method (Sculpture/Softness)
- Optimize parameter
- Resolution scale of Cloud rendering for optimization
- Animated switching of profile
- HeightFog renders far space
- Reduce shader keyword
- Add HDRP shader variants (for Unity 2018.3)
- Dual layered Clouds rendering
- Add Phase shift feature

- Fix: Unexpectedly changed Camera Depth mode
- Fix: [VR] Jitter with SinglePass rendering

--------------
Version 2.3.0

- Shadow casting (Screen Space Shadow)
- Add Dissolve Parameter
- Fix: Height Fog was distorted

--------------
Version 2.2.1 (2019/3/10)

- Add MassiveCloudsMaterialExporter

--------------
Version 2.2.0 (2019/3/8)

- Dimming Sun by Global Lighting
- Edge Lighting
- Linear Color Space Adjustment
- Brightness and Contrast Feature
- Constant Light Power Feature
- Renew Inspector GUI
- Fix wrong Tiling Mode given at VolumeTextures
- Fix calculation of transparency with scene objects correctly

--------------
Version 2.1.0 (2019/2/3)

- VR Ready (Oculus Rift / Google Cardboard / Daydream)
- Octave Phaser

--------------
Version 2.0.0 (2019/1/16)

- Octave Feature
- Solid Lighting Calculation
- Android Support (OpenGL ES 3.0)
- [preview] VR support using CameraEffect Mode and MultiPass


--------------
Version 1.2.0 (2018/12/23)

- Brush up HeightLimitation Feature
- HDRP Directional Lighting Support
- Add BeyondCloudsDemo Scene


--------------
Version 1.1.0 (2018/12/15)

- Add HeightLimitation Feature
- Add Light Opaqueness Feature
- Overhaul shader codes


[Contact]

Unity Connect

  https://connect.unity.com/u/59113a1132b3060016aea0e6

Unity Forum

  https://forum.unity.com/threads/v4-0-0-released-massive-clouds-screen-space-volumetric-clouds.581182/

Twitter

  @mewlist

Web

  https://mewli.st/

Email
  mewlist@mewlist.com

