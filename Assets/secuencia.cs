using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class secuencia : MonoBehaviour
{
    public Material sky;
    public Texture tex;
    public Texture tex2;
    void Start()
    {
        sky.mainTexture = tex;
        StartCoroutine(sequence());
    }

    IEnumerator sequence()
    {
        yield return new WaitForSecondsRealtime(18f);
        sky.mainTexture = tex2;
    }
}
