using Mewlist;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class helicopter : MonoBehaviour
{
    public MassiveClouds clouds;
    public Material sky;
    public Texture tex;
    public Texture tex2;
    public Texture tex3;
    public Texture tex4;
    public Texture tex5;
    public Texture tex6;
    public Texture tex7;
    public Texture tex8;
    public Texture tex9;
    public List<MassiveCloudsProfile> currentParameters = new List<MassiveCloudsProfile>();
    public List<MassiveCloudsProfile> lessClouds = new List<MassiveCloudsProfile>();
    public List<MassiveCloudsProfile> Clouds = new List<MassiveCloudsProfile>();
    public GameObject station;
    // Start is called before the first frame update
    void Start()
    {
        sky.mainTexture = tex;
        StartCoroutine(sequence());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator sequence()
    {
        yield return new WaitForSecondsRealtime(0f);
        station.SetActive(true);
        yield return new WaitForSecondsRealtime(0f);
        clouds.SetProfiles(lessClouds);
        yield return new WaitForSecondsRealtime(40f);
        clouds.SetProfiles(currentParameters);
        yield return new WaitForSecondsRealtime(1f);
        sky.mainTexture = tex2;
        yield return new WaitForSecondsRealtime(1f);
        clouds.SetProfiles(lessClouds);
        yield return new WaitForSecondsRealtime(22f);
        clouds.SetProfiles(currentParameters);
        yield return new WaitForSecondsRealtime(1f);
        sky.mainTexture = tex3;
        yield return new WaitForSecondsRealtime(1f);
        clouds.SetProfiles(lessClouds);
        yield return new WaitForSecondsRealtime(13f);
        clouds.SetProfiles(currentParameters);
        yield return new WaitForSecondsRealtime(1f);
        sky.mainTexture = tex4;
        yield return new WaitForSecondsRealtime(1f);
        clouds.SetProfiles(lessClouds);
        yield return new WaitForSecondsRealtime(8f);
        clouds.SetProfiles(currentParameters);
        yield return new WaitForSecondsRealtime(1f);
        sky.mainTexture = tex5;
        yield return new WaitForSecondsRealtime(1f);
        clouds.SetProfiles(lessClouds);
        yield return new WaitForSecondsRealtime(18f);
        clouds.SetProfiles(currentParameters);
        yield return new WaitForSecondsRealtime(1f);
        sky.mainTexture = tex6;
        yield return new WaitForSecondsRealtime(1f);
        clouds.SetProfiles(lessClouds);
        yield return new WaitForSecondsRealtime(24f);
        clouds.SetProfiles(currentParameters);
        yield return new WaitForSecondsRealtime(1f);
        sky.mainTexture = tex7;
        yield return new WaitForSecondsRealtime(1f);
        clouds.SetProfiles(lessClouds);
        yield return new WaitForSecondsRealtime(16f);
        clouds.SetProfiles(currentParameters);
        yield return new WaitForSecondsRealtime(1f);
        sky.mainTexture = tex8;
        yield return new WaitForSecondsRealtime(1f);
        clouds.SetProfiles(lessClouds);
        yield return new WaitForSecondsRealtime(25f);
        clouds.SetProfiles(currentParameters);
        yield return new WaitForSecondsRealtime(1f);
        sky.mainTexture = tex9;
        yield return new WaitForSecondsRealtime(1f);
        clouds.SetProfiles(lessClouds);
        yield return new WaitForSecondsRealtime(28f);
        station.SetActive(true);
        yield return new WaitForSecondsRealtime(2f);
        clouds.SetProfiles(lessClouds);
    }
}
