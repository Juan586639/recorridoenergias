using System.Diagnostics;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

namespace Evereal.VideoCapture.Editor
{
  /// <summary>
  /// Inspector script for <c>VideoCapture</c> component.
  /// </summary>
  [CustomEditor(typeof(VideoCapture))]
  public class VideoCaptureInspector : UnityEditor.Editor
  {
    VideoCapture videoCapture;
    SerializedProperty inputTexture;
    SerializedProperty cursorImage;

    public void OnEnable()
    {
      videoCapture = (VideoCapture)target;
      inputTexture = serializedObject.FindProperty("inputTexture");
      cursorImage = serializedObject.FindProperty("cursorImage");
    }

    public override void OnInspectorGUI()
    {
      // Capture Control Section
      GUILayout.Label("Capture Control", EditorStyles.boldLabel);

      serializedObject.Update();

      videoCapture.startOnAwake = EditorGUILayout.Toggle("Start On Awake", videoCapture.startOnAwake);
      if (videoCapture.startOnAwake)
      {
        videoCapture.captureTime = EditorGUILayout.FloatField("Capture Duration (Sec)", videoCapture.captureTime);
        videoCapture.quitAfterCapture = EditorGUILayout.Toggle("Quit After Capture", videoCapture.quitAfterCapture);
      }

      // Capture Options Section
      GUILayout.Label("Capture Options", EditorStyles.boldLabel);

      videoCapture.captureSource = (CaptureSource)EditorGUILayout.EnumPopup("Capture Source", videoCapture.captureSource);

      if (videoCapture.captureSource == CaptureSource.RENDERTEXTURE)
      {
        EditorGUILayout.PropertyField(inputTexture, new GUIContent("Render Texture"), true);
      }

      videoCapture.captureType = (CaptureType)EditorGUILayout.EnumPopup("Capture Type", videoCapture.captureType);
      if (videoCapture.captureType == CaptureType.VOD)
      {
        videoCapture.saveFolder = EditorGUILayout.TextField("Save Folder", videoCapture.saveFolder);
      }
      else if (videoCapture.captureType == CaptureType.LIVE)
      {
        videoCapture.liveStreamUrl = EditorGUILayout.TextField("Live Stream Url", videoCapture.liveStreamUrl);
      }
      if (videoCapture.captureSource == CaptureSource.CAMERA)
      {
        videoCapture.captureMode = (CaptureMode)EditorGUILayout.EnumPopup("Capture Mode", videoCapture.captureMode);

        if (videoCapture.captureMode == CaptureMode._360)
        {
          videoCapture.projectionType = (ProjectionType)EditorGUILayout.EnumPopup("Projection Type", videoCapture.projectionType);
        }

        if (videoCapture.captureMode == CaptureMode._360 &&
          videoCapture.projectionType == ProjectionType.CUBEMAP)
        {
          videoCapture.stereoMode = StereoMode.NONE;
        }
        else
        {
          videoCapture.stereoMode = (StereoMode)EditorGUILayout.EnumPopup("Stereo Mode", videoCapture.stereoMode);
        }
        if (videoCapture.stereoMode != StereoMode.NONE)
        {
          videoCapture.interpupillaryDistance = EditorGUILayout.FloatField("Interpupillary Distance", videoCapture.interpupillaryDistance);
        }
      }
      else
      {
        videoCapture.captureMode = CaptureMode.REGULAR;
        videoCapture.projectionType = ProjectionType.NONE;
        videoCapture.stereoMode = StereoMode.NONE;
      }

      if (videoCapture.captureSource == CaptureSource.SCREEN)
      {
        videoCapture.captureCursor = EditorGUILayout.Toggle("Capture Cursor", videoCapture.captureCursor);
        if (videoCapture.captureCursor)
        {
          EditorGUILayout.PropertyField(cursorImage, new GUIContent("Cursor Image"), true);
        }
      }

      videoCapture.captureAudio = EditorGUILayout.Toggle("Capture Audio", videoCapture.captureAudio);
      videoCapture.captureMicrophone = EditorGUILayout.Toggle("Capture Microphone", videoCapture.captureMicrophone);
      videoCapture.offlineRender = EditorGUILayout.Toggle("Offline Render", videoCapture.offlineRender);

      // Capture Options Section
      GUILayout.Label("Video Settings", EditorStyles.boldLabel);

      if (videoCapture.captureSource == CaptureSource.CAMERA)
      {
        videoCapture.resolutionPreset = (ResolutionPreset)EditorGUILayout.EnumPopup("Resolution Preset", videoCapture.resolutionPreset);
        if (videoCapture.resolutionPreset == ResolutionPreset.CUSTOM)
        {
          videoCapture.frameWidth = EditorGUILayout.IntField("Frame Width", videoCapture.frameWidth);
          videoCapture.frameHeight = EditorGUILayout.IntField("Frame Height", videoCapture.frameHeight);
          videoCapture.bitrate = EditorGUILayout.IntField("Bitrate (Kbps)", videoCapture.bitrate);
        }
      }
      
      videoCapture.frameRate = (System.Int16)EditorGUILayout.IntField("Frame Rate", videoCapture.frameRate);
      if (videoCapture.captureMode == CaptureMode._360)
      {
        videoCapture.cubemapFaceSize = (CubemapFaceSize)EditorGUILayout.EnumPopup("Cubemap Face Size", videoCapture.cubemapFaceSize);
      }
      videoCapture.antiAliasingSetting = (AntiAliasingSetting)EditorGUILayout.EnumPopup("Anti Aliasing Settings", videoCapture.antiAliasingSetting);

      // Capture Options Section
      GUILayout.Label("Encoder Components", EditorStyles.boldLabel);

      videoCapture.softwareEncodingOnly = EditorGUILayout.Toggle("Software Encoding Only", videoCapture.softwareEncodingOnly);

      // Tools Section
      GUILayout.Label("Tools", EditorStyles.boldLabel);

      if (GUILayout.Button("Open Save Folder"))
      {
        // Open video save directory
        Process.Start(Config.saveFolder);
      }
      if (GUILayout.Button("Convert Last Video to WMV"))
      {
        Utils.ConvertVideoWmv(Config.lastVideoFile);
      }
      if (GUILayout.Button("Convert Last Video to AVI"))
      {
        Utils.ConvertVideoAvi(Config.lastVideoFile);
      }
      if (GUILayout.Button("Convert Last Video to FLV"))
      {
        Utils.ConvertVideoFlv(Config.lastVideoFile);
      }
      if (GUILayout.Button("Convert Last Video to MKV"))
      {
        Utils.ConvertVideoMkv(Config.lastVideoFile);
      }
      if (GUILayout.Button("Convert Last Video to GIF"))
      {
        Utils.ConvertVideoGif(Config.lastVideoFile);
      }
      if (GUILayout.Button("Encode Last Video to 4K"))
      {
        Utils.EncodeVideo4K(Config.lastVideoFile);
      }
      if (GUILayout.Button("Encode Last Video to 8K"))
      {
        Utils.EncodeVideo8K(Config.lastVideoFile);
      }

      if (GUI.changed)
      {
        EditorUtility.SetDirty(target);
        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
      }

      // Apply changes to the serializedProperty - always do this at the end of OnInspectorGUI.
      serializedObject.ApplyModifiedProperties();
    }
  }
}