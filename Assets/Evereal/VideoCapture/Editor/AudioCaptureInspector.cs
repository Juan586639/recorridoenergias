﻿using System.Diagnostics;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

namespace Evereal.VideoCapture.Editor
{
  /// <summary>
  /// Inspector script for <c>AudioCapture</c> component.
  /// </summary>
  [CustomEditor(typeof(AudioCapture))]
  public class AudioCaptureInspector : UnityEditor.Editor
  {
    AudioCapture audioCapture;

    public void OnEnable()
    {
      audioCapture = (AudioCapture)target;
    }

    public override void OnInspectorGUI()
    {
      // Capture Control Section
      GUILayout.Label("Capture Control", EditorStyles.boldLabel);

      audioCapture.startOnAwake = EditorGUILayout.Toggle("Start On Awake", audioCapture.startOnAwake);
      if (audioCapture.startOnAwake)
      {
        audioCapture.captureTime = EditorGUILayout.FloatField("Capture Duration (Sec)", audioCapture.captureTime);
        audioCapture.quitAfterCapture = EditorGUILayout.Toggle("Quit After Capture", audioCapture.quitAfterCapture);
      }

      // Capture Options Section
      GUILayout.Label("Capture Options", EditorStyles.boldLabel);

      audioCapture.saveFolder = EditorGUILayout.TextField("Save Folder", audioCapture.saveFolder);

      // Tools Section
      GUILayout.Label("Tools", EditorStyles.boldLabel);

      if (GUILayout.Button("Open Save Folder"))
      {
        // Open video save directory
        Process.Start(Config.saveFolder);
      }

      if (GUI.changed)
      {
        EditorUtility.SetDirty(target);
        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
      }
    }
  }
}