﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using UnityEngine;

namespace Evereal.VideoCapture
{
  // This script will merge video/audio to flv and publish to remote streaming service.
  public class FFmpegStreamer : MonoBehaviour
  {
    #region Dll Import

    [DllImport("FFmpegEncoder")]
    private static extern IntPtr FFmpegEncoder_StartMuxingProcess(int rate,
                                                                  string path,
                                                                  string vpath,
                                                                  string apath,
                                                                  string ffpath,
                                                                  bool live);

    [DllImport("FFmpegEncoder")]
    private static extern void FFmpegEncoder_CleanMuxingProcess(IntPtr api);

    //[DllImport("FFmpegEncoder")]
    //private static extern IntPtr FFmpegEncoder_StartLiveStreamProcess(string streamUrl,
    //                                                                  string videoPath,
    //                                                                  string ffmpegPath);

    //[DllImport("FFmpegEncoder")]
    //private static extern void FFmpegEncoder_CleanLiveStreamProcess(IntPtr api);

    #endregion

    #region Properties

    public static FFmpegStreamer singleton;

    // Live stream url
    public string liveStreamUrl = "";
    // Video bitrate
    public Int32 bitrate = 2000;

    // Live stream is already started
    public bool streamStarted { get; private set; }

    // Video slice for live streaming.
    private Queue<string> videoSliceQueue;
    // Audio slice for live streaming.
    private Queue<string> audioSliceQueue;
    // Flv live streaming video slice.
    private Queue<string> liveSliceQueue;

    // The audio/video mux thread.
    private Thread muxingThread;

    // The live streaming thread.
    private Thread streamThread;

    private string ffmpegPath;

    // Log message format template
    //private string LOG_FORMAT = "[FFmpegStreamer] {0}";

    #endregion

    #region Live Streaming

    public bool StartStream()
    {
      // Check if we can start stream session
      if (streamStarted)
        return false;

      // Check if live stream url is correct set
      if (string.IsNullOrEmpty(liveStreamUrl))
        return false;

      // Reset slice queue
      videoSliceQueue = new Queue<string>();
      audioSliceQueue = new Queue<string>();
      liveSliceQueue = new Queue<string>();

      ffmpegPath = Config.ffmpegPath;

      if (streamThread != null && streamThread.IsAlive)
      {
        streamThread.Abort();
        streamThread = null;
      }

      streamStarted = true;

      // Start video/audio thread.
      if (muxingThread != null)
      {
        if (muxingThread.IsAlive)
          muxingThread.Abort();
        muxingThread = null;
      }
      muxingThread = new Thread(MuxingThreadFunction);
      muxingThread.Priority = System.Threading.ThreadPriority.Lowest;
      muxingThread.IsBackground = true;
      muxingThread.Start();

      // Start live stream thread.
      if (streamThread != null)
      {
        if (streamThread.IsAlive)
          streamThread.Abort();
        streamThread = null;
      }
      streamThread = new Thread(LiveStreamThreadFunction);
      streamThread.Priority = System.Threading.ThreadPriority.Lowest;
      streamThread.IsBackground = true;
      streamThread.Start();

      return true;
    }

    public bool StopStream()
    {
      if (!streamStarted)
        return false;

      streamStarted = false;

      return true;
    }

    public void EnqueueVideoSlice(string slice)
    {
      lock (this)
      {
        videoSliceQueue.Enqueue(slice);
      }
    }

    public void EnqueueAudioSlice(string slice)
    {
      lock (this)
      {
        audioSliceQueue.Enqueue(slice);
      }
    }

    // Muxing video/audio in thread
    private void MuxingThreadFunction()
    {
      while (streamStarted || (videoSliceQueue.Count > 0 && audioSliceQueue.Count > 0))
      {
        if (videoSliceQueue.Count > 0 && audioSliceQueue.Count > 0)
        {
          string videoSlice;
          string audioSlice;
          lock (this)
          {
            videoSlice = videoSliceQueue.Dequeue();
            audioSlice = audioSliceQueue.Dequeue();
          }
          string ext = Path.GetExtension(videoSlice);
          string liveSlice = videoSlice.Replace(ext, ".mp4");

          IntPtr muxingAPI = FFmpegEncoder_StartMuxingProcess(bitrate, liveSlice, videoSlice, audioSlice, ffmpegPath, true);

          while (!File.Exists(liveSlice))
          {
            // Wait 10 millisecond for muxing
            Thread.Sleep(10);
          }

          FFmpegEncoder_CleanMuxingProcess(muxingAPI);

          if (streamStarted)
          {
            liveSliceQueue.Enqueue(liveSlice);
          }
          else
            File.Delete(liveSlice);

          // Clean temp file
          if (File.Exists(videoSlice))
            File.Delete(videoSlice);
          if (File.Exists(audioSlice))
            File.Delete(audioSlice);
        }
      }
    }

    // Live streaming process in thread
    private void LiveStreamThreadFunction()
    {
      while (streamStarted || liveSliceQueue.Count > 0)
      {
        if (liveSliceQueue.Count > 0)
        {
          string liveSlice;
          lock (this)
          {
            liveSlice = liveSliceQueue.Dequeue();
          }
          Process process = new Process();
          ProcessStartInfo startInfo = new ProcessStartInfo();
          startInfo.WindowStyle = ProcessWindowStyle.Hidden;
          // startInfo.CreateNoWindow = true;
          startInfo.FileName = ffmpegPath;
          startInfo.Arguments = "-re -i " + liveSlice + " -deinterlace -vcodec libx264 -preset ultrafast -tune zerolatency -bufsize 512k -f flv " + liveStreamUrl;
          process.StartInfo = startInfo;
          process.Start();
          process.WaitForExit();
          process.Close();
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
          // Wait until live slice file consumed
          Thread.Sleep(FFmpegEncoder.LIVE_SLICE_SECONDS * 1000);
#endif
          // Clean live video file
          if (File.Exists(liveSlice))
            File.Delete(liveSlice);
        }
        else if (streamStarted)
        {
          // Wait 10 millisecond for captured frame
          Thread.Sleep(10);
        }
        else
        {
          break;
        }
      }
      // Clean temp file
      CleanTempFiles();
    }

    private void CleanTempFiles()
    {
      while (videoSliceQueue.Count > 0)
      {
        string videoSlice;
        lock (this)
        {
          videoSlice = videoSliceQueue.Dequeue();
        }
        if (File.Exists(videoSlice))
          File.Delete(videoSlice);
      }
      while (audioSliceQueue.Count > 0)
      {
        string audioSlice;
        lock (this)
        {
          audioSlice = audioSliceQueue.Dequeue();
        }
        if (File.Exists(audioSlice))
          File.Delete(audioSlice);
      }
    }

    private void Awake()
    {
      if (singleton != null)
        return;
      singleton = this;

      streamStarted = false;
    }

    #endregion
  }
}