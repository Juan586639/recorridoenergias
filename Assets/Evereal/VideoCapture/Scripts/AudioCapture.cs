using System;
using System.IO;
using UnityEngine;

namespace Evereal.VideoCapture
{
  /// <summary>
  /// This script will record target audio listener sample and encode to audio file, or mux audio into video file if required.
  /// </summary>
  [Serializable]
  public class AudioCapture : MonoBehaviour, IAudioCapture
  {
    #region Properties

    // Start capture on awake if set to true.
    [SerializeField]
    public bool startOnAwake = false;
    // Quit process after capture finish.
    [SerializeField]
    public bool quitAfterCapture = false;
    // The capture duration if start capture on awake.
    [SerializeField]
    public float captureTime = 30f;

    [Tooltip("Save folder for recorded audio")]
    // Save path for recorded video including file name (c://xxx.wav)
    [SerializeField]
    public string saveFolder = "";

    // Capture microphone settings
    [SerializeField]
    public bool captureMicrophone = false;

    // Callback for complete handling
    public event AudioCaptureCompleteEvent OnComplete = delegate { };
    // Callback for error handling
    public event AudioCaptureErrorEvent OnError = delegate { };

    // Is audio capture started
    public bool captureStarted
    {
      get
      {
        return FFmpegMuxer.singleton.captureStarted;
      }
    }

    // Log message format template
    private string LOG_FORMAT = "[AudioCapture] {0}";

    #endregion

    #region Methods

    // Start capture audio session
    public bool StartCapture()
    {
      if (!FFmpegMuxer.singleton)
      {
        OnError(this, CaptureErrorCode.AUDIO_CAPTURE_START_FAILED);
        return false;
      }

      // Check if we can start capture session
      if (captureStarted)
      {
        OnError(this, CaptureErrorCode.AUDIO_CAPTURE_ALREADY_IN_PROGRESS);
        return false;
      }

      if (string.IsNullOrEmpty(saveFolder))
        saveFolder = Config.saveFolder;
      else
        Config.saveFolder = saveFolder;

      // init audio encoder settings
      FFmpegMuxer.singleton.captureMic = captureMicrophone;

      FFmpegMuxer.singleton.StartCapture();

      Debug.LogFormat(LOG_FORMAT, "Audio capture session started.");

      return true;
    }

    // Stop capture audio session
    public bool StopCapture()
    {
      if (!captureStarted)
      {
        Debug.LogFormat(LOG_FORMAT, "Audio capture session not start yet!");
        return false;
      }

      FFmpegMuxer.singleton.StopCapture();

      OnComplete(this, FFmpegMuxer.singleton.audioSavePath);

      Debug.LogFormat(LOG_FORMAT, "Audio capture session success!");

      return true;
    }

    // Cancel capture audio session
    public bool CancelCapture()
    {
      if (!captureStarted)
      {
        Debug.LogFormat(LOG_FORMAT, "Audio capture session not start yet!");
        return false;
      }

      FFmpegMuxer.singleton.CancelCapture();

      Debug.LogFormat(LOG_FORMAT, "Audio capture session canceled!");

      return true;
    }

    #endregion

    #region Unity Lifecycle

    private void Awake()
    {
      if (!FFmpegMuxer.singleton)
      {
        AudioListener listener = FindObjectOfType<AudioListener>();
        if (!listener)
        {
          Debug.LogFormat(LOG_FORMAT, "AudioListener not found, disable audio capture!");
        }
        else
        {
          listener.gameObject.AddComponent<FFmpegMuxer>();
        }
      }

      if (startOnAwake)
      {
        StartCapture();
      }
    }

    private void Update()
    {
      if (startOnAwake)
      {
        if (Time.time >= captureTime && captureStarted)
        {
          StopCapture();
        }
        if (quitAfterCapture && !captureStarted)
        {
#if UNITY_EDITOR
          UnityEditor.EditorApplication.isPlaying = false;
#else
          Application.Quit();
#endif
        }
      }
    }

    private void OnDestroy()
    {
      // Check if still processing on destroy
      if (captureStarted)
      {
        StopCapture();
      }
    }

    private void OnApplicationQuit()
    {
      // Check if still processing on application quit
      if (captureStarted)
      {
        StopCapture();
      }
    }

    #endregion
  }
}